<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('password', 'App\Http\Controllers\MarketlinkusController@password');
    Route::post('welcome', 'App\Http\Controllers\MarketlinkusController@welcome');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
      //


    });
});

