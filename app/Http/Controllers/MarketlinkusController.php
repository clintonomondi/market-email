<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Market_password;
use App\Mail\Welcome;
use Illuminate\Support\Facades\Mail;

class MarketlinkusController extends Controller
{
    public function password(Request $request){

        Mail::to($request->email)->send(new Market_password($request));
        return ['status'=>true,'message'=>'Message sent successfully'];
    }
    public function welcome(Request $request){

        Mail::to($request->email)->send(new Welcome($request));
        return ['status'=>true,'message'=>'Message sent successfully'];
    }
}
